# -*- coding: utf-8 -*-
import csv
rating=10
NEGATIONS_WORDS = ["aint", "arent", "cannot", "cant", "couldnt", "darent", "didnt", "doesnt",
                     "ain't", "aren't", "can't", "couldn't", "daren't", "didn't", "doesn't",
                     "dont", "hadnt", "hasnt", "havent", "isnt", "mightnt", "mustnt", "neither",
                     "don't", "hadn't", "hasn't", "haven't", "isn't", "mightn't", "mustn't",
                     "neednt", "needn't", "never", "none", "nope", "nor", "not", "nothing", "nowhere",
                     "oughtnt", "shant", "shouldnt", "uhuh", "wasnt", "werent",
                     "oughtn't", "shan't", "shouldn't", "wasn't", "weren't",
                     "without", "wont", "wouldnt", "won't", "wouldn't", "rarely", "seldom", "despite"]


with open('stopWords1.csv', 'r') as f:
            reader = csv.reader(f)

            inp2 = list(reader)

            StopWord = inp2[0]


WordsDict = {}
with open('Positive.csv', 'r') as f:
        reader = csv.reader(f)
        WordList = list(reader)

print (WordList[1])
for a in WordList:
    WordsDict[str(a[0]).lower()] = (1.0,)
    print (a)
print (*WordsDict)

with open('Negative.csv', 'r') as f:
    reader = csv.reader(f)
    WordList = list(reader)








for row in WordList:
    WordsDict[str(row[0]).lower()] = (-1.0,)

print (WordsDict['ponzi'])

with open('nouns.csv', 'r') as f:
            reader = csv.reader(f)
            inp = list(reader)
            Nouns = inp[0]

def solve(rev):
        FeatureDict = {}
        avg = 0
        cnt = 0
        # test = []
        # Fptr : Feature detected with index
        # Adjptr: Adjective detected with polarity and index

        for sentence in rev.split('.'):
            # print(sentence)
            Fptr = []
            AdjPtr = []
            if sentence == '':
                continue
            arr = sentence.split(' ')
            i = 0
            direction = +1
            for cstr in arr:
                print(cstr+'\n')
                i += 1
                p = str(cstr).lower()
                if p in NEGATIONS_WORDS:
                    direction = -direction
                    continue
                if p in StopWord:
                    continue
                if p == "but":
                    direction = +1
                    continue
                elif p in Nouns:
                    Fptr.append((p, i))
                    if p not in FeatureDict.keys():
                        FeatureDict[p] = 0.0
                        if p in WordsDict.keys():
                            FeatureDict[p] = WordsDict[p][0] * 0.1
                elif p in WordsDict.keys():
                    x = WordsDict[p][0]
                    if x > 0.125 or x < -0.125:
                        AdjPtr.append((p, direction * WordsDict[p][0], i))
                        # print AdjPtr

                        # Pairing Adjectives and Nouns by computing distance.
                        # The adjectasdsadsadsaive and noun with minimum distance between them will be paired.

            for i in range(len(AdjPtr)):
                dist = 1000000007
                feat = ""
                for j in range(len(Fptr)):
                    if abs(AdjPtr[i][2] - Fptr[j][1]) < dist:
                        dist = abs(AdjPtr[i][2] - Fptr[j][1])
                        feat = Fptr[j][0]
                if feat != '':
                    FeatureDict[feat] += AdjPtr[i][1]
                avg += AdjPtr[i][1]
                cnt += 1
                # test.append(AdjPtr[i][1])
                # print feat + " - " + AdjPtr[i][0] + " || " ,(AdjPtr[i][1])


                # print FeatureDict

                # Computing the Score

        avg /= max(cnt, 1)

        # nscore = (x - minscore)/(maxscore - minscore)
        # handle rating part( thats left for computing the score)
        # If score > 2.5 , sentiment = Positive else sentiment = Negative

        normalizedscore = ((avg + 1) / 2) * 5

        # Taking 30% rating and 70% Score computed
        if rating >= 0:
            fscore = normalizedscore * 0.7 + 0.3 *rating
        else:
            fscore = normalizedscore * 1.0


        FinanceList = []
        LogisticsList = []
        QualityList = []

        LogisticsDept = ["slow", "behind", "belated", "blown", "delayed", "dilatory", "eleventh-hour", "gone", "jammed",
                         "lagging", "overdue", "postponed", "remiss", "stayed", "tardy", "late", "unpunctual",
                         "aboriginal", "antecedent", "antediluvian", "antiquated", "preceding", "premier", "prevenient",
                         "primal", "prime", "primitive", "primordial", "prior", "pristine", "proleptical", "raw",
                         "undeveloped", "gradual", "heavy", "lackadaisical", "leisurely", "lethargic", "moderate",
                         "passive", "quiet", "reluctant", "sluggish", "stagnant", "crawling", "creeping", "dawdling",
                         "delaying", "deliberate", "disinclined", "idle", "lagging", "loitering", "measured",
                         "plodding", "postponing", "procrastinating", "slack", "apathetic", "dilatory", "dreamy",
                         "drowsy", "imperceptible", "inactive", "indolent", "leaden", "negligent", "slow-moving",
                         "snaillike", "torpid", "tortoiselike"]

        FinanceDept = ["costly", "extravagant", "high", "lavish", "overpriced", "pricey", "valuable", "excessive",
                       "exorbitant", "immoderate", "inordinate", "invaluable", "posh", "rich", "swank", "uneconomical",
                       "unreasonable", "economical", "reasonable", "worthless", "cheap", "inexpensive", "moderate",
                       "agile", "nimble", "accelerated", "electric", "flashing", "flying", "snap", "winged",
                       "breakneck", "highprice", "price", "priced", "cost"]

        QualityDept = ["complicated", "confusing", "flawed", "imprecise", "inaccurate", "incomplete", "incorrect",
                       "useful", "not ", "pointless", "ugly", "unfinished", "unreliable", "useless", "appropriate",
                       "attractive", "convenient", "faultless", "flawless", "free of error", "handy", "helpful ",
                       "practical", "precise", "quality", "useful ", "acceptable", "bad", "excellent", "exceptional",
                       "favorable", "great", "marvelous", "positive", "satisfactory", "satisfying", "superb",
                       "valuable", "wonderful", "ace", "boss", "bully", "capital", "choice", "crack", "nice",
                       "pleasing", "prime", "rad", "sound", "spanking", "sterling", "super", "superior", "welcome",
                       "worthy", "admirable", "agreeable", "commendable", "congenial", "deluxe", "first-class",
                       "first-rate", "gratifying", "honorable", "neat", "precious", "reputable", "select", "shipshape",
                       "splendid", "stupendous", "poor", "dreadful", "atrocious", "cheap", "unacceptable", "sad",
                       "lousy", "crummy", "awful", "rough", "synthetic", "gross", "imperfect", "bummer", "garbage",
                       "blah", "diddly", "inferior", "downer", "abominable", "amiss", "bad news", "beastly", "careless",
                       "cheesy", "crappy", "defective", "deficient", "satisfactory", "erroneous", "fallacious",
                       "faulty", "grungy", "icky", "inadequate", "incorrect", "off", "raunchy", "slipshod", "stinking",
                       "substandard", "unsatisfactory", "junky", "bottom out", "not good"]

        # Initializing the Lists
        FinanceList.append(0)
        LogisticsList.append(0)
        QualityList.append(0)

        a = rev
        v = a.split('.')
        # print "V " ,v
        S = 0
        for sentence in v:
            S = S + 1
            z = sentence.split(' ')
            # print z,S
            for c in z:
                c = c.lower()
                if c in FinanceDept and S not in FinanceList:
                    FinanceList.append(S)
                if c in LogisticsDept and S not in LogisticsList:
                    LogisticsList.append(S)
                if c in QualityDept and S not in QualityList:
                    QualityList.append(S)

        return fscore
print (solve(''' There is so much hype about alt-coins lately that there are now reports of people even taking out second mortgages and home equity lines to buy them. The volatility is so great that the Chicago Board Options Exchange (CBOE) halted bitcoin trading twice on Dec. 10 and once again on Dec. 13, and Coinbase halted litecoin and ethereum trading on Dec. 12.

For years, financial analysts have warned people away from cryptocurrency by arguing that it was too volatile to be a safe investment. However, with prices going sky-high, it's hard for investors and entrepreneurs to sit on the sidelines while a major new asset class emerges.

However, before people take the plunge, they need to understand the risks. The cryptocurrency markets aren't just volatile, they are also extremely murky and riddled with fraud. Since the launch of bitcoin in 2009, these markets have been plagued with cyber attacks and scams that have cost investors millions of dollars. To make matters worse, cryptocurrency isn't protected by the FDIC, so losses due to theft may not be covered.

Related: 5 Essential Podcasts for Entrepreneurs Serious About Cryptocurrency

There are two main ways cryptocurrency investors can lose their shirts to scammers.
'''))
